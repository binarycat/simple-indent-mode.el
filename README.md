## usage
call `simple-indent-set-string` or `simple-indent-autodetect` in the hook of the
mode you want to override tab behavior of.

if you want an default indentation string in case the autodetect fails (such as
for new/empty files), call `simple-indent-set-string` with the desired default,
then call `simple-indent-autodetect`.

## default keybinds
* TAB: increase indentation of selected lines by 1
* SHIFT+TAB: decrease indentation of selected lines by 1
* NEWLINE: insert a new line and copies over the leading whitespace of the previous line by one

## concept
the current intentation system of emacs is complicated:
1. the autoformatter 
frequently activates when you don't want it to, and will fight back if you try
to override it.
2. there is no option to indent with only tabs, indentation is done either via
tabs, or with tabs + spaces.  the only way this is functional in most major
modes is by the autoformatting only ever trying to indent to a multiple of the current tab width.
3. the underlying configuration state is complicated, currently there are 5 variables in the `indent` customization group.

this minor mode simplifies this down into 1 buffer-local variable and 3 indentation aware commands:
* simple-indent-string: stores the indentation string for the current buffer.  one instance of this string at the start of a line is one indentation level
* simple-indent-tab: adds another instance of the indentation string to the start of each selected line
* simple-indent-untab: removes an instance of the indentation string from the start of each selected line (will never delete non-whitespace characters)
* simple-indent-newline: inserts a newline followed by the leading whitespace of the current line

