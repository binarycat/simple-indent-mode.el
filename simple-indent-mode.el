; TODO: custom? groups?

(defgroup simple-indent nil
  "Customization options for simple-indent-mode"
  :group 'indent)

(defcustom simple-indent-autodetect-max-chars 5000
  "maximum number of chars at the start of file to try to find the first indent

this exists so that large files that contain no indentation will not cause undue lag"
  :type '(integer)
  :group 'simple-indent)

(defcustom simple-indent-modeline "SimpleIndent"
  "string to display as part of the mode line"
  :type '(string)
  :group 'simple-indent)

(defcustom simple-indent-comment-faces '(font-lock-comment-face font-lock-doc-face)
  "list of faces that are considered comments for the purpouse of 'simple-indent-autodetect'"
  :group 'simple-indent)

(defvar simple-indent-string "\t"
  "string representing one level of indentation")
(defun simple-indent-endws ()
  "returns the position of the first non-whitespace charachter in the current line"
  (save-excursion
    (beginning-of-line)
    ;; we don't need a limit, on an empty line we hit a \n or eof
    (skip-chars-forward " \t")
    (point)))

(defun simple-indent-describe ()
  (cond ((string-match "\s+" simple-indent-string)
		 (format "%d" (length simple-indent-string)))
		((string-match "\t+" simple-indent-string)
		 (if (= 1 (length simple-indent-string))
			 "t"
		   (format "%dt" (length simple-indent-string))))
		(t "?")))
		 
	

(defun simple-indent-set-string (s)
  (simple-indent-mode t)
  ;; todo: describe the indentation that was set, either with the mode-line or via a message
  (setq-local simple-indent-string s))

(defun simple-indent-eachln (fun)
  "apply the specified function to each line in the region"
  (if (use-region-p)
	  (let ((start (region-beginning))
			(end (region-end)))
		(save-excursion
		  (goto-char start)
		  (dotimes (lineno (count-lines start end))
			(funcall fun)
			(forward-line 1))
		  (setq deactivate-mark nil)
		  (activate-mark)))
	  (funcall fun)))

;;; interactive commands

(defun simple-indent-autodetect ()
  "automatically detect the indentation to use in the current buffer"
  (interactive)
  (save-excursion
	(goto-char (point-min))
	;; find the first indented non-comment line that contains non whitespace
	(while (progn
			 (re-search-forward "^\\([[:space:]]+\\)[^\s\t\n]" simple-indent-autodetect-max-chars)
			 (member (face-at-point) simple-indent-comment-faces)))
	(simple-indent-set-string (match-string 1))))

(defun simple-indent-tab ()
   "increase the level of indentation by one stage"
   (interactive)
   (save-excursion
	 (simple-indent-eachln
      (lambda ()
        (beginning-of-line)
        (insert simple-indent-string))))
   (unless (use-region-p)
	 (goto-char (max (point) (simple-indent-endws)))))

(defun simple-indent-untab ()
  "decrease the level of indentation by one stage"
  (interactive)
  (simple-indent-eachln
   (lambda ()
	 (save-excursion
	   (beginning-of-line)
	   (let ((len (max 0 (min (length simple-indent-string)
							  (- (simple-indent-endws) (point))))))
		 (delete-char len))))))

(defun simple-indent-newline ()
  "insert a newline followed by the leading whitespace of the current line"
  (interactive)
  (let ((lws (buffer-substring (line-beginning-position) (simple-indent-endws))))
    (insert "\n")
    (insert lws)))
  
(define-minor-mode simple-indent-mode
  "minor mode to simplify indentation.

it completly bypasses the usual complicated indentation system of emacs, including any autoformatting done by the current major mode."
  :group 'simple-indent
  :lighter (" " simple-indent-modeline "[" (:eval (simple-indent-describe)) "]")
  :keymap `((,(kbd "<tab>") . simple-indent-tab)
			(,(kbd "<backtab>") . simple-indent-untab)
			(,(kbd "<return>") . simple-indent-newline)))

(add-hook 'simple-indent-hook
		  (lambda ()
			(setq-local c-electric-flag nil)
			(setq-local c-syntactic-indentation nil)))
								
